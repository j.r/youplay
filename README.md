# ![Logo](img/youplay_logo.png) Welcome to YouPlay

Do you love music, but hate restrictions?

With **YouPlay** you can search for whatever music in your mind. It will list a number of matching songs from YouTube. You can listen to it and **YouPlay** will download it as an mp3 file.

##### YouPlay in GUI-mode

![YouPlay Screenshot GUI](img/screenshot_gui.png)

##### YouPlay in CLI-mode

![YouPlay Screenshot CLI](img/screenshot_cli.png)

## Installation

You can install the Python-Script, generate a Flatpak or try the AppImage:

- I recommend to install **YouPlay** manually as described in the next chapter.

- A Flatpak package is ready, but not distributed to Flathub.org because I don't want to feed Microsofts GitHub, which is required for Flathub.org publications. All files for the Flatpak are available in this repository, but you need to know how to build a Flatpak.

- J.r maintains an [AUR package](https://aur.archlinux.org/packages/youplay)

- @ProfP303 created AppImages for various versions: check the AppImage files

#### Manual Installation

This installation method is still the preferred one, because it is easy and has the smallest installation size. Run these commands in a terminal:

```bash
git clone https://codeberg.org/ralfhersel/youplay.git
cd youplay
```

Then install these packages with the package manager of your GNU/Linux distribution:

        mpv, libmpv1, ffmpeg, python3-pip

- On Debian-based systems you can do this with following command:

        sudo apt install mpv libmpv1 ffmpeg python3-pip

- On Arch-based systems:

        sudo pacman -S mpv ffmpeg python-pip

- On Fedora-based systems:

        sudo dnf install mpv mpv-libs ffmpeg pip3

- On openSUSE-based systems:

        sudo zypper install mpv libmpv1 ffmpeg libadwaita-1-0 yt-dlp python3-pympv

Now install the necessary Python libraries with:

        pip install -r requirements.txt

Note: If you are on a Qt-based desktop you might want to use the Qt-version instead (this will install the official "Qt for Python" aka PySide6-bindings by the Qt Company). For this see following chapter ["Qt-port Installation"](#qt-port-installation)

Last steps:

- Make `youplay.py` and `youplay.sh` executable in your file manager
- Change the paths (Exec and Icon) in `youplay.desktop` to your paths
  (e.g.: `/home/yourpath/youplay`). Please replace the Exec and Icon settings with absolute paths to your YouPlay directory. Example:

        Exec=/home/ralf/dev/youplay/youplay.sh -g

        Icon=/home/ralf/dev/youplay/youplay.svg

- Copy `youplay.desktop` to `/home/user/.local/share/applications`

Test it:

- GNOME and KDE-Plasma: SUPER + youplay

- Terminal: navigate to `/home/user/[your_script_folder]/youplay/`

    Run it with: `./youplay.sh`

    If you get GUI or TUI depends on the settings in `youplay.sh`

Note: Alternatively you could use the full-automated online-installer from the shell (only `wget` has to be installed):

    bash <(wget -O - https://codeberg.org/ralfhersel/youplay/raw/scripts/online-install)

If installed via online-installer, you can uninstall YouPlay with following command: `youplay-uninstall`

#### AppImage Installation

Thanks to @ProfP303, there is an AppImage available. This image works on several desktop environments (Plasma, GNOME, LXQt, Cinnamon, MATE) if GTK4 and Libadwaita are available. Here are commands to install them:

- Arch Plasma: `sudo pacman -S gtk4 libadwaita`

- Fedora Plasma: `sudo dnf install gtk4 libadwaita`

- Debian 12 ("Bookworm") Plasma: `sudo apt install libgtk-4-dev libadwaita-1-dev` 

- openSUSE Leap Plasma: `sudo zypper install gtk4-devel libadwaita-devel` 

You can download an AppImage from the [Releases](https://codeberg.org/ralfhersel/youplay/releases)-page, make it executable and run it. If you need a better integration in the GNOME-Desktop, take a look at [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher). Install it before you launch the AppImage with AppImageLauncher.

[Here](https://codeberg.org/ralfhersel/youplay/releases/tag/v0.38) you can find also an older AppImage, which is based on GTK3, in case your distribution does not support GTK4 and Libadwaita. The features are almost the same.

#### Qt-port Installation

The Qt-port version uses the Python-bindings for the Qt GUI-Toolkit.

        pip install -r requirements_qt.txt

Optionally you may use the PyQt6-bindings by Riverbank Computing:

        pip install -r requirements_pyqt6.txt

- Make `youplay_qt.py` executable in your file manager

- Run it with: `./youplay_qt.py` or `./youplay_pyqt6.py` respectively

- Optionally change the Exec-command of your `youplay.desktop`-file in `/home/user/.local/share/applications`

Note #1: In case you have installed the Qt-version adapt the Exec-command like this:

        Exec=/home/ralf/dev/youplay/youplay.sh --gui=qt

Note #2: In case you have installed the PyQt6-version adapt the Exec-command like this:

        Exec=/home/ralf/dev/youplay/youplay.sh --gui=pyqt6

## Operation Instructions

YouPlay supports two modes: CLI (command line mode) and GUI (graphical mode).

You can start YouPlay in CLI mode like this:

- Navigate in a terminal to your YouPlay directory: `cd /home/blabla/youplay`

    ./youplay.py

    ./youplay.py --help

    ./youplay.py songtitle

All other options will be shown interactively.

You can also start YouPlay in GUI mode:

- Navigate in a terminal to your YouPlay directory: `cd /home/blabla/youplay`

    ./youplay.sh

- or from your application menu or launcher (this requires `youplay.desktop` with adapted paths, copied to `/home/user/.local/share/applications/`)

Note: in case you have installed YouPlay via online-installer you might run it from everywhere, no need to `cd` into a special directory first.

When the GUI shows up, you can enter a song title (min 4 characters) that you want to listen to. The App will present a list of 10 matching songs from Youtube. Select one or doubleclick to start downloading and playing it. All songs that you are listened to, are stored as mp3 files in a subfolder named 'youplay' under your standard music folder in home. A song, that was already downloaded, will not be downloaded again, as long as the file exists in the 'youplay' subfolder. You can show and play all downloaded songs with the SONG button. Just enter another search term to return to the search list.

## Problems

Since Youtube changes the API every now and then, it is recommended to update `yt-dlp` regularly:

```bash
yt-dlp -U
```
## License

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/youplay.git

## Contact

ralf.hersel@gmx.net
