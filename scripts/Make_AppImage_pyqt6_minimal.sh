#!/usr/bin/env bash
# Project:        youplay
# File:           scripts/Make_AppImage.sh
# Author:         Prof.P
# Licence:        GPL3
#
# Make AppImage from Python program with Python-AppImage
# ARCH string can be "x86_64" or "i686"
# -----------------------------------------------------------------------
VERSION="0.46"
ARCH="x86_64"
APPIMAGENAME="youplay_v$VERSION-PyQt6_$ARCH-minimal.AppImage"
StartDir=$PWD
BaseDir=$(dirname "$StartDir")
AppDir=$BaseDir/AppDir
DistDir=$BaseDir/dist
TmpDir=$DistDir/_tmp
LogFile=$BaseDir/dist/Make_AppImage.log
mkdir -p $DistDir && mkdir -p $TmpDir
# -----------------------------------------------------------------------
echo -e "Make AppImage from Python\n" |tee $LogFile
read -p "Continue with ENTER . . ."
echo -e "-----------------------------------------------------------------------" |tee -a $LogFile

# -----------------------------------------------------------------------
# remove previous build files (suppress not-existing error)
rm -rf $AppDir &>/dev/null

# -----------------------------------------------------------------------
# download the necessary tools.
echo -e "[1/7] - Downloading the necessary tool." |tee -a $LogFile
wget https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-$ARCH.AppImage -O $TmpDir/appimagetool &>> $LogFile
# make it executable so that we can call it
chmod a+x $TmpDir/appimagetool

cd $BaseDir

# create our app AppImage structure
echo -e "[2/7] - Creating our app AppImage structure." |tee -a $LogFile
mkdir $AppDir/
cp youplay.py $AppDir/
cp youplay_pyqt6.py $AppDir/
cp youplay.svg $AppDir/
cp youplay.desktop $AppDir/
mkdir $AppDir/img/ && cp img/icons8-search-50.png $AppDir/img/

# Create our AppRun launcher script
echo -e "[3/7] - Creating our AppRun launcher script." |tee -a $LogFile
cat >> $AppDir/AppRun << 'EOF'
#!/usr/bin/env bash

# If running from an extracted image, then export ARGV0 and APPDIR
if [ -z "${APPIMAGE}" ]; then
    export ARGV0="$0"

    self=$(readlink -f -- "$0") # Protect spaces (issue 55)
    here="${self%/*}"
    export APPDIR=$here
fi

cd $APPDIR

if [ -z "$@" ]
then # if no command line argument was passed, assume GUI mode
      "python3" "youplay.py" "--gui=pyqt6"
else # launch youplay with command line args
      "python3" "youplay.py" "$@"
fi
EOF
chmod a+x $AppDir/AppRun

# replace icon in desktop-file (necessary for AppImage compliance)
echo -e "[4/7] - Replacing icon in desktop-file (necessary for AppImage compliance)." |tee -a $LogFile
sed -i 's/^Icon=.*/Icon=youplay/' $AppDir/youplay.desktop

# repackage the newly created AppImage
echo -e "[5/7] - Repackaging the newly created AppImage." |tee -a $LogFile
ARCH=$ARCH $TmpDir/appimagetool $AppDir $APPIMAGENAME |tee -a $LogFile

# remove temp files
echo -e "[6/7] - Removing temporary files." |tee -a $LogFile
rm -rf $TmpDir

# move appimage to dist dir
echo -e "[7/7] - Moving AppImage to dir $DistDir." |tee -a $LogFile
mkdir -p $DistDir
mv $APPIMAGENAME $DistDir

# go back to where we started from
cd $StartDir

# display end message
echo -e "\nDone creating $DistDir/$APPIMAGENAME." |tee -a $LogFile
echo -e "=======================================================================" |tee -a $LogFile
echo -e "(This window can be closed.)"

# make notification sound
if command -v 'paplay' &>/dev/null; then
  if test -f "/usr/share/sounds/freedesktop/stereo/complete.oga"; then
    paplay "/usr/share/sounds/freedesktop/stereo/complete.oga"
  fi
fi
