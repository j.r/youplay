#!/usr/bin/env bash

#
# Name:           install.sh
# Project:        youplay
# Licence:        GPL3
# Repository:     https://codeberg.org/ralfhersel/youplay
#

echo
echo "YouPlay installation script 'install.sh'"
echo
echo "Superuser-permissions needed in order to install necessary packages!"
echo "--------------------------------------------------------------------"

PKG_MGR="$1"

# define the youplay folder
YOUPLAY_BASE_DIR=$(dirname "$PWD")

# define our installation directory for the programm
YOUPLAY_INSTALL_DIR="/usr/share/youplay"

# if empty argument passed...
if [ -z "$PKG_MGR" ]; then # ...try to guess the package manager
    # Install non-pip dependencies
    if [[ -f /etc/manjaro-release ]]; then
        PKG_MGR="--pamac"
        echo "Found known distro Manjaro. :-)"
    elif [[ -f /etc/arch-release ]]; then
        echo "Found known distro Arch (or Arch-based). :-)"
        PKG_MGR="--pacman"
    elif [[ -f /etc/debian_version ]]; then
        echo "Found known distro Debian (or Debian-based). :-)"
        PKG_MGR="--apt"
    elif [[ -f /etc/redhat-release ]]; then
        echo "Found known distro Fedora or Red Hat. :-)"
        PKG_MGR="--dnf"
    elif [[ -f /etc/os-release ]]; then
        DISTRO=$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release)
        if [[ $DISTRO == \""opensuse-leap\"" ]]; then
            echo "Found known distro openSUSE. :-)"
            PKG_MGR="--zypper"
        fi
    else
      echo "Cannot auto-detect distro. :-("
    fi
fi

# remove program directory
sudo rm -rf /usr/local/share/youplay/
sudo rm -rf "$YOUPLAY_INSTALL_DIR"

# delete symbolic link
sudo rm -f /usr/local/bin/youplay
sudo rm -f /usr/bin/youplay
sudo rm -f /usr/bin/youplay-uninstall

# remove desktop launcher file
sudo rm -f /usr/share/applications/youplay.desktop

if [ "$PKG_MGR" == "--pamac" ]; then
    echo "Pamac-CLI is used as package manager."
    echo
    read -rp "Press ENTER to continue or Ctrl+C to cancel."
    echo "Starting.."
    sudo mkdir -pv $YOUPLAY_INSTALL_DIR && sudo touch $YOUPLAY_INSTALL_DIR/pamac.type
    pamac install mpv ffmpeg python-pip libadwaita
elif [ "$PKG_MGR" == "--pacman" ]; then
    echo "Pacman is used as package manager."
    echo
    read -rp "Press ENTER to continue or Ctrl+C to cancel."
    echo "Starting.."
    sudo mkdir -pv $YOUPLAY_INSTALL_DIR && sudo touch $YOUPLAY_INSTALL_DIR/pacman.type
    sudo pacman -S --noconfirm mpv ffmpeg python-pip libadwaita yt-dlp
elif [ "$PKG_MGR" == "--apt" ]; then
    echo "APT is used as package manager."
    echo
    read -rp "Press ENTER to continue or Ctrl+C to cancel."
    echo "Starting.."
    sudo mkdir -pv $YOUPLAY_INSTALL_DIR && sudo touch $YOUPLAY_INSTALL_DIR/apt.type
    sudo apt install mpv libmpv-dev ffmpeg python-gi-dev python3-mpv yt-dlp
elif [ "$PKG_MGR" == "--dnf" ]; then
    echo "DNF is used as package manager."
    echo
    read -rp "Press ENTER to continue or Ctrl+C to cancel."
    echo "Starting.."
    sudo mkdir -pv $YOUPLAY_INSTALL_DIR && sudo touch $YOUPLAY_INSTALL_DIR/dnf.type
    sudo dnf install mpv mpv-libs ffmpeg-free python3-pip python3-mpv libadwaita
elif [ "$PKG_MGR" == "--zypper" ]; then
    echo "Zypper is used as package manager."
    echo
    read -rp "Press ENTER to continue or Ctrl+C to cancel."
    echo "Starting.."
    sudo mkdir -pv $YOUPLAY_INSTALL_DIR && sudo touch $YOUPLAY_INSTALL_DIR/zypper.type
    sudo zypper install -y mpv libmpv1 ffmpeg libadwaita-1-0 yt-dlp python3-pympv
else
    echo "Please specify which package-manager to use:"
    echo
    echo " --pamac   Use 'Pamac' (CLI) for installing"
    echo " --pacman  Use 'Pacman' for installing"
    echo " --apt     Use 'APT' for installing"
    echo " --dnf     Use 'DNF' for installing"
    echo " --zypper  Use 'Zypper' for installing"
    echo
    exit
fi

# copy necessary files for execution
sudo cp -v "$YOUPLAY_BASE_DIR"/youplay.py $YOUPLAY_INSTALL_DIR
sudo cp -v "$YOUPLAY_BASE_DIR"/youplay_*.py $YOUPLAY_INSTALL_DIR
sudo cp -v "$YOUPLAY_BASE_DIR"/requirements.txt $YOUPLAY_INSTALL_DIR
sudo cp -v "$YOUPLAY_BASE_DIR"/youplay.svg $YOUPLAY_INSTALL_DIR
sudo cp -vr "$YOUPLAY_BASE_DIR"/scripts/ $YOUPLAY_INSTALL_DIR/scripts/
sudo cp -vr "$YOUPLAY_BASE_DIR"/img/ $YOUPLAY_INSTALL_DIR/img/

# copy desktop launcher file
sudo cp -v "$YOUPLAY_BASE_DIR"/youplay.desktop /usr/share/applications/

# create symbolic link to be able to call the python script like a binary
sudo ln -s $YOUPLAY_INSTALL_DIR/youplay.py /usr/bin/youplay
sudo ln -s $YOUPLAY_INSTALL_DIR/scripts/uninstall.sh /usr/bin/youplay-uninstall

# install necessary pip packages
if [ "$PKG_MGR" != "--apt" ] && [ "$PKG_MGR" != "--zypper" ] && [ "$PKG_MGR" != "--pacman" ]
then
	python3 -m pip install -r ../requirements.txt
fi

echo "Done installing $(youplay --version)"
exit
