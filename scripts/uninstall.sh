#!/usr/bin/env bash

# Name:           uninstall.sh
# Project:        youplay
# Licence:        GPL3
# Repository:     https://codeberg.org/ralfhersel/youplay

echo
echo "YouPlay uninstallation script 'uninstall.sh'"
echo
echo "Superuser-permissions needed in order to remove necessary packages!"
echo "--------------------------------------------------------------------"

read -rp "Press ENTER to continue or Ctrl+C to cancel."

if ! command -v youplay --version &> /dev/null
then
	echo "Youplay is not installed."
	exit 1
fi

YOUPLAY_VERION=$(youplay --version)

# define our installation directory for the programm
YOUPLAY_INSTALL_DIR="/usr/share/youplay"

# remove pip packages
if [[ -f $YOUPLAY_INSTALL_DIR/apt.type ]]; then
   :
elif [[ -f $YOUPLAY_INSTALL_DIR/zypper.type ]]; then
   :
elif [[ -f $YOUPLAY_INSTALL_DIR/pacman.type ]]; then
   :
else
	echo "Executing pip uninstall"
	python3 -m pip uninstall -r $YOUPLAY_INSTALL_DIR/requirements.txt
fi

# remove program directory
sudo rm -rf /usr/local/share/youplay/
sudo rm -rf $YOUPLAY_INSTALL_DIR


# delete symbolic link
sudo rm -f /usr/local/bin/youplay
sudo rm -f /usr/bin/youplay

# remove desktop launcher file
sudo rm -f /usr/share/applications/youplay.desktop

echo -e "Done removing $YOUPLAY_VERION"
exit
